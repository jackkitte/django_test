from django.urls import path
from . import views

app_name = 'manager'
urlpatterns = [
    path('worker_list/', views.WorkerListView.as_view(), name='index'),
]